package com.example.gpstracker;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	ImageView image;
	Bitmap bitmap;
	Bitmap emptyMap;
	TextView gpsStatus;
	TextView xy_pos;	
	Button boundary_btn;
	Button tracker_btn;
	Button analyze_btn;
	Location last_location=null;
	float total_distence=0;
	float last_distence=0;
	long last_time=0;
	long begin_time=0;
	
	File mGpsFile;
	File mGpsBorderFile;
	FileOutputStream mPrintStream = null;
    boolean record = false;
    boolean record_border = false;
    boolean generate_border = false;
    BoundaryFilter mBoundary;
    int record_count = 0;
    Bitmap drawmap;
    Canvas canvas;
    Paint paint;	
    int rate = 50;
    List<double[]> poselist;
    
	Handler handler = new Handler(){
		public void handleMessage(Message msg){
			String message=(String)msg.obj;
			if(message.compareTo("loadsucc")==0){
				System.out.println("setImageBitmap");
				image.setImageBitmap(bitmap);
				image.invalidate();
			}
			if(message.compareTo("boundaryOK")==0){
				System.out.println("boundaryOK");
				tracker_btn.setEnabled(true);
				Toast.makeText(getApplicationContext(), "generate boundary success! Now start tracking", Toast.LENGTH_SHORT).show();
				Toast.makeText(getApplicationContext(), "width:"+mBoundary.width+" height:"+mBoundary.height, Toast.LENGTH_SHORT).show();
			}
			if(message.compareTo("boundaryFailed")==0){
				System.out.println("boundaryOK");
				Toast.makeText(getApplicationContext(), "generate boundary failed, please try again!", Toast.LENGTH_SHORT).show();				
			}
		}
	};
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
  		
        poselist = new ArrayList<double[]>();
        
        image = (ImageView) findViewById(R.id.image);
        drawmap = BitmapFactory.decodeResource(getResources(),R.drawable.football_background);
        //bitmap = image.loadLocationData3(null,drawmap.copy(Bitmap.Config.ARGB_8888, true));
        //image.setImageBitmap(bitmap);
        image.setImageBitmap(drawmap);
        emptyMap = BitmapFactory.decodeResource(getResources(),R.drawable.empty);
        
		paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setAlpha(200);
		paint.setStrokeWidth(10);
        gpsStatus = (TextView) findViewById(R.id.gps_status);
        xy_pos = (TextView) findViewById(R.id.xy_pos);
        final Runnable loadimagethread = new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("start load imge");
				long begin = System.currentTimeMillis();
				byte[] inOutb = null;
				//read file
				try {
					FileInputStream reader = new FileInputStream(mGpsFile);
					inOutb = new byte[reader.available()];
					reader.read(inOutb); 
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(inOutb!=null && inOutb.length>80){
					List<BoundaryFilter.PlanePosition> locationlist = new ArrayList<BoundaryFilter.PlanePosition>();		
					for(int i=0,n=inOutb.length/16;i<n;i++){
						double x = CommonUtil.getDouble(inOutb,i*16);
						double y = CommonUtil.getDouble(inOutb,(i*16+8));
						//System.out.print("getfromfile latitude:"+latitude+", longitude:"+longitude);
						locationlist.add(mBoundary.new PlanePosition((int)x,(int)y));
					}
					bitmap = mBoundary.loadLocationData(locationlist,BitmapFactory.decodeResource(getResources(),R.drawable.football_background).copy(Bitmap.Config.ARGB_8888, true));
					Message msg = new Message();
					msg.obj = "loadsucc";
					handler.sendMessage(msg);
				}
				System.out.println("load image takes:"+(System.currentTimeMillis()-begin)+"ms");
			}
        	
        };        
        
        final Runnable analyzeBoundarythread = new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("start analyze boundary");
				long begin = System.currentTimeMillis();
				boolean result = false;
				if(mBoundary!=null){
					try {
						result = mBoundary.analyze();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						xy_pos.setText("excetion:"+e.getMessage());
					}
				}
				if(result){
					Message msg = new Message();
					msg.obj = "boundaryOK";
					handler.sendMessage(msg);
				}
				else{
					Message msg = new Message();
					msg.obj = "boundaryFailed";
					handler.sendMessage(msg);
				}		
				if(poselist.size()>0){
					Bitmap temp = emptyMap.copy(Bitmap.Config.ARGB_8888, true);
	                canvas = new Canvas(temp);
	                double[] front = poselist.get(0);
	                paint.setColor(Color.GREEN);
	        		for(int i=1,n=poselist.size();i<n;i++){
	        			double[] pp1 = poselist.get(i);               			
	        			canvas.drawCircle((float)(pp1[0]-front[0])*10+temp.getWidth()/2, (float)(pp1[1]-front[1])*10+temp.getHeight()/2, 
	        					10, paint);
	        		}
	        		       			
	        		paint.setColor(Color.RED);	  	
	        		canvas.drawCircle((float)(mBoundary.left_top[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.left_top[1]-front[1])*10+temp.getHeight()/2, 
	    					10, paint);
	        		paint.setColor(Color.YELLOW);
	        		canvas.drawCircle((float)(mBoundary.right_top[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.right_top[1]-front[1])*10+temp.getHeight()/2, 
	    					10, paint);
	        		paint.setColor(Color.BLUE);
	        		canvas.drawCircle((float)(mBoundary.right_bottom[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.right_bottom[1]-front[1])*10+temp.getHeight()/2, 
	    					10, paint);
	        		paint.setColor(Color.BLACK);        		
	        		canvas.drawCircle((float)(mBoundary.left_bottom[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.left_bottom[1]-front[1])*10+temp.getHeight()/2, 
	    					10, paint);
	        		
	        		image.setImageBitmap(temp);     
	        		image.invalidate();
				}
				System.out.println("analyze takes:"+(System.currentTimeMillis()-begin)+"ms");
			}
        	
        };        
        LocationManager locationManager = (LocationManager) this.getSystemService(getApplicationContext().LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
            	double[] xy=null;
            	xy = CommonUtil.GaussToBLToGauss(location.getLongitude(),location.getLatitude());
                gpsStatus.setText("location : " + location.getLatitude() + "," +
                        location.getLongitude() +", records:"+record_count);
                if(record_border){
                	mBoundary.addGpsData(xy);
                	record_count++;
                	poselist.add(xy);
                	if(record_count>3){
                		Bitmap temp = emptyMap.copy(Bitmap.Config.ARGB_8888, true);
                        canvas = new Canvas(temp);
                        double[] front = poselist.get(0);
                		for(int i=1,n=poselist.size();i+1<n;i++){
                			double[] pp1 = poselist.get(i);
                			double[] pp2 = poselist.get(i+1);                			
                			canvas.drawLine((float)(pp1[0]-front[0])*10+temp.getWidth()/2, (float)(pp1[1]-front[1])*10+temp.getHeight()/2, 
                					(float)(pp2[0]-front[0])*10+temp.getWidth()/2, (float)(pp2[1]-front[1])*10+temp.getHeight()/2, paint);
                		}
                		image.setImageBitmap(temp);     
                		image.invalidate();
                	}
                }
                else if(record){
                	//caculate total distence
                	float current_distence = 0;
                	long current_time = System.currentTimeMillis()/1000;
                	if(last_location!=null){
                		current_distence = location.distanceTo(last_location);
                		total_distence = total_distence+current_distence; 
                	}
                	else{
                		begin_time = current_time;
                		last_time = current_time;
                		last_distence = total_distence;
                	}
                	last_location = location;
                	//caculate the speed                	
                	if(record_count%5==0){
                		float speed = ((total_distence-last_distence)*60/(current_time-last_time));
                		xy_pos.setText("Total distence|time:"+(int)total_distence+" (meters)|"+(current_time-begin_time)/60+" (min)\n"+
                		               "Current Speed :"+(int)speed+" (meter/min)\n"+
                					   "Average Speed :"+(int)(total_distence*60/(current_time-begin_time))+" (meter/min)");
                		last_time = current_time;
                		last_distence = total_distence;
                	}
                	
                	if(mPrintStream!=null){	
                		record_count++;
                		double x = xy[0];
                		double y = xy[1];
                			
                		int result = mBoundary.adjust_xy(xy);
                		if(result==0){              			
                			//xy_pos.setText("in x:"+(int)xy[0]+", y:"+(int)xy[1]);
                			Bitmap temp = drawmap.copy(Bitmap.Config.ARGB_8888, true);
                            canvas = new Canvas(temp);
                            paint.setColor(Color.BLUE);
                    		canvas.drawCircle((float)xy[0], (float)xy[1], 20, paint);
                    		image.setImageBitmap(temp);     
                    		image.invalidate();                    		
                    		try {
    							mPrintStream.write(CommonUtil.getBytes(xy[0]));
    							mPrintStream.write(CommonUtil.getBytes(xy[1]));
    						} catch (IOException e1) {
    							// TODO Auto-generated catch block
    							e1.printStackTrace();
    						}
                        }  
                		else{
                			double[] front = poselist.get(0);
                			xy_pos.setText("out result:"+result+" x:"+(int)xy[0]+", y:"+(int)xy[1]+" width:"+
                					mBoundary.width+", height:"+mBoundary.height+
                					" left_top["+(int)(mBoundary.left_top[0]-front[0])+","+(int)(mBoundary.left_top[1]-front[1])+"],"+
                					" right_top["+(int)(mBoundary.right_top[0]-front[0])+","+(int)(mBoundary.right_top[1]-front[1])+"],"+
                					" right_bottom["+(int)(mBoundary.right_bottom[0]-front[0])+","+(int)(mBoundary.right_bottom[1]-front[1])+"],"+
                					" left_bottom["+(int)(mBoundary.left_bottom[0]-front[0])+","+(int)(mBoundary.left_bottom[1]-front[1])+"]");
                			
                			Bitmap temp = emptyMap.copy(Bitmap.Config.ARGB_8888, true);
        	                canvas = new Canvas(temp);
        	                
        	                paint.setColor(Color.BLUE);
        	        		canvas.drawCircle((float)(x-front[0])*10+temp.getWidth()/2, (float)(y-front[1])*10+temp.getHeight()/2, 
        	        					10, paint);
        	        		paint.setColor(Color.RED);
        	        		canvas.drawCircle((float)(mBoundary.left_top[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.left_top[1]-front[1])*10+temp.getHeight()/2, 
        	    					10, paint);
        	        		paint.setColor(Color.YELLOW);
        	        		canvas.drawCircle((float)(mBoundary.right_top[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.right_top[1]-front[1])*10+temp.getHeight()/2, 
        	    					10, paint);
        	        		paint.setColor(Color.BLUE);
        	        		canvas.drawCircle((float)(mBoundary.right_bottom[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.right_bottom[1]-front[1])*10+temp.getHeight()/2, 
        	    					10, paint);
        	        		paint.setColor(Color.BLACK);        		
        	        		canvas.drawCircle((float)(mBoundary.left_bottom[0]-front[0])*10+temp.getWidth()/2, (float)(mBoundary.left_bottom[1]-front[1])*10+temp.getHeight()/2, 
        	    					10, paint);
        	        		
        	        		image.setImageBitmap(temp);     
        	        		image.invalidate();
        	        		
                		}
                		
                	}
                }
                else{
                	begin_time = 0;
                	last_time = 0;
                	last_location = null;
                	total_distence = 0;
                	record_count = 0;
                	if(mPrintStream!=null){
                		try {
							mPrintStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                		mPrintStream=null;
                	}
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                System.out.println("=========== onStatusChanged : " + provider + " status : " + status);
            }

            public void onProviderEnabled(String provider) {
                System.out.println("=========== onProviderEnabled : " + provider);
            }

            public void onProviderDisabled(String provider) {
                System.out.println("=========== onProviderDisabled : " + provider);
            }
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListener);

        mGpsFile = new File(getApplicationContext().getFilesDir(), "TestGpsLocation.dat");
        
        //
        
        analyze_btn = (Button) findViewById(R.id.load_btn);
        analyze_btn.setEnabled(false);
        analyze_btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				//load data and draw
				handler.post(loadimagethread); 
			}
        });
        
        boundary_btn = (Button) findViewById(R.id.border_btn);
        boundary_btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				//load data and draw
				if(record_border){
					record_border = false;
					Button btn = (Button) arg0;
					btn.setText("CreateBorder");	
					//start analyze
					handler.post(analyzeBoundarythread);					
					
            		//image.setImageBitmap(drawmap);     
            		//image.invalidate();
				}
				else{
					record_border = true;
					Button btn = (Button) arg0;
					btn.setText("Finish");				
					
					if(mBoundary!=null){
						mBoundary.clean();
					}
					mBoundary = new BoundaryFilter(new File(getApplicationContext().getFilesDir(), "TestGpsBorderLocation.dat"),drawmap.getWidth(),drawmap.getHeight());
					tracker_btn.setEnabled(false);
					analyze_btn.setEnabled(false);
					Toast.makeText(getApplicationContext(), "start record, please run a rectangle for boundary", Toast.LENGTH_SHORT).show();
					/*					
					double[] xy = {3,1};
					mBoundary.addGpsData(xy);
					double[] xy1 = {4,3};
					mBoundary.addGpsData(xy1);
					double[] xy2 = {5,2};
					mBoundary.addGpsData(xy2);
					double[] xy3 = {3,5};
					mBoundary.addGpsData(xy3);
					double[] xy4 = {6,5};
					mBoundary.addGpsData(xy4);
					double[] xy5 = {8,4};
					mBoundary.addGpsData(xy5);
					double[] xy6 = {5,7};
					mBoundary.addGpsData(xy6);
					double[] xy7 = {2,6};
					mBoundary.addGpsData(xy7);
					double[] xy8 = {1,4};
					mBoundary.addGpsData(xy8);	
					mBoundary.analyze();					
					
					System.out.println("left_top:"+mBoundary.left_top[0]+" y:"+mBoundary.left_top[1]);
					System.out.println("right_top:"+mBoundary.right_top[0]+" y:"+mBoundary.right_top[1]);
					System.out.println("right_bottom:"+mBoundary.right_bottom[0]+" y:"+mBoundary.right_bottom[1]);
					System.out.println("left_bottom:"+mBoundary.left_bottom[0]+" y:"+mBoundary.left_bottom[1]);
					*/
					/*
					double[] tt = {-100,-100};
					for(int i=0;i<10;i++){
						double[] xy = {i,0};
						CommonUtil.move(xy, tt);
						CommonUtil.rotate(xy,-30*Math.PI/180);
						mBoundary.addGpsData(xy);
					}
					for(int i=1;i<10;i++){
						double[] xy = {0,i};
						CommonUtil.move(xy, tt);
						CommonUtil.rotate(xy,-30*Math.PI/180);
						mBoundary.addGpsData(xy);
					}
					for(int i=0;i<10;i++){
						double[] xy = {i,10};
						CommonUtil.move(xy, tt);
						CommonUtil.rotate(xy,-30*Math.PI/180);
						mBoundary.addGpsData(xy);
					}
					for(int i=0;i<10;i++){
						double[] xy = {10,i};
						CommonUtil.move(xy, tt);
						CommonUtil.rotate(xy,-30*Math.PI/180);
						mBoundary.addGpsData(xy);
					}					
					mBoundary.analyze();					
					
					double[] test = {10,8};
					CommonUtil.move(test, tt);
					CommonUtil.rotate(test,-30*Math.PI/180);
					int result = mBoundary.adjust_xy(test);
					System.out.println("left_top:"+mBoundary.left_top[0]+" y:"+mBoundary.left_top[1]);
					System.out.println("right_top:"+mBoundary.right_top[0]+" y:"+mBoundary.right_top[1]);
					System.out.println("right_bottom:"+mBoundary.right_bottom[0]+" y:"+mBoundary.right_bottom[1]);
					System.out.println("left_bottom:"+mBoundary.left_bottom[0]+" y:"+mBoundary.left_bottom[1]);
					
					System.out.println("test result:"+result+", x:"+test[0]+" y:"+test[1]);
					*/
				}
			}
        });
        
        tracker_btn = (Button) findViewById(R.id.record_btn);
        tracker_btn.setEnabled(false);
        tracker_btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				if(record){
					record = false;
					Button btn = (Button) arg0;
					btn.setText("StartTrack");
					
            		image.setImageBitmap(drawmap);     
            		image.invalidate();
            		record_count = 0;
            		analyze_btn.setEnabled(true);
				}
				else{
					record = true;
					Button btn = (Button) arg0;
					btn.setText("FinishTrack");				
					analyze_btn.setEnabled(false);
					try {
						mPrintStream = new FileOutputStream(mGpsFile);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
        });
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if(mPrintStream!=null){
			try {
				mPrintStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mPrintStream=null;
		}
	}
	
	double[] MCT84Bl2xy(double l, double B)
    {
		double[] output = new double[2];
        try
        {
        	l = l * Math.PI /180;
            B = B * Math.PI /180;

            double B0 =30* Math.PI /180;

            double N =0, e =0, a =0, b =0, e2 =0, K =0;
            a =6378137;
            b =6356752.3142;
            e = Math.sqrt(1- (b / a) * (b / a));
            e2 = Math.sqrt((a / b) * (a / b) -1);
            double CosB0 = Math.cos(B0);
            N = (a * a / b) / Math.sqrt(1+ e2 * e2 * CosB0 * CosB0);
            K = N * CosB0;

            double Pi = Math.PI;
            double SinB = Math.sin(B);

            double tan = Math.tan(Pi /4+ B /2);
            double E2 = Math.pow((1- e * SinB) / (1+ e * SinB), e /2);
            double xx = tan * E2;

            output[0] = K * Math.log(xx);
            output[1] = K * l;
            return output;
        }
        catch (Exception ErrInfo)
        {
        }
        output[0] = 0;
        output[1] = 0;
        return output;
    }
	
}
