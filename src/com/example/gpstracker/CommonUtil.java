package com.example.gpstracker;

public class CommonUtil {
	
	public static byte[] getBytes(double d)
    {
		long data = Double.doubleToLongBits(d);
        byte[] bytes = new byte[8];
        bytes[0] = (byte) (data & 0xff);
        bytes[1] = (byte) ((data >> 8) & 0xff);
        bytes[2] = (byte) ((data >> 16) & 0xff);
        bytes[3] = (byte) ((data >> 24) & 0xff);
        bytes[4] = (byte) ((data >> 32) & 0xff);
        bytes[5] = (byte) ((data >> 40) & 0xff);
        bytes[6] = (byte) ((data >> 48) & 0xff);
        bytes[7] = (byte) ((data >> 56) & 0xff);
        return bytes;
    }
	
	public static double getDouble(byte[] b, int index) { 
	    long l; 
	    l = b[index]; 
	    l &= 0xff; 
	    l |= ((long) b[index+1] << 8); 
	    l &= 0xffff; 
	    l |= ((long) b[index+2] << 16); 
	    l &= 0xffffff; 
	    l |= ((long) b[index+3] << 24); 
	    l &= 0xffffffffl; 
	    l |= ((long) b[index+4] << 32); 
	    l &= 0xffffffffffl; 
	    l |= ((long) b[index+5] << 40); 
	    l &= 0xffffffffffffl; 
	    l |= ((long) b[index+6] << 48); 
	    l &= 0xffffffffffffffl; 
	    l |= ((long) b[index+7] << 56); 
	    return Double.longBitsToDouble(l); 
	} 
	
	public static double[] GaussToBLToGauss(double longitude, double latitude)
	{
		 double[] output = new double[2];
		 int ProjNo=0; int ZoneWide; ////带宽
		 double longitude1,latitude1, longitude0,latitude0, X0,Y0, xval,yval;
		 double a,f, e2,ee, NN, T,C,A, M, iPI;
		 iPI = 0.0174532925199433; ////3.1415926535898/180.0;
		 ZoneWide = 6; ////6度带宽
		 a=6378245.0; f=1.0/298.3; //54年北京坐标系参数
		 ////a=6378140.0; f=1/298.257; //80年西安坐标系参数
		 ProjNo = (int)(longitude / ZoneWide) ;
		 longitude0 = ProjNo * ZoneWide + ZoneWide / 2;
		 longitude0 = longitude0 * iPI ;
		 latitude0 = 0;
		 //System.out.println(latitude0);
		 longitude1 = longitude * iPI ; //经度转换为弧度
		 latitude1 = latitude * iPI ; //纬度转换为弧度
		 e2=2*f-f*f;
		 ee=e2*(1.0-e2);
		 NN=a/Math.sqrt(1.0-e2*Math.sin(latitude1)*Math.sin(latitude1));
		 T=Math.tan(latitude1)*Math.tan(latitude1);
		 C=ee*Math.cos(latitude1)*Math.cos(latitude1);
		 A=(longitude1-longitude0)*Math.cos(latitude1);
		 M=a*((1-e2/4-3*e2*e2/64-5*e2*e2*e2/256)*latitude1-(3*e2/8+3*e2*e2/32+45*e2*e2
		 *e2/1024)*Math.sin(2*latitude1)
		 +(15*e2*e2/256+45*e2*e2*e2/1024)*Math.sin(4*latitude1)-(35*e2*e2*e2/3072)*Math.sin(6*latitude1));
		 xval = NN*(A+(1-T+C)*A*A*A/6+(5-18*T+T*T+72*C-58*ee)*A*A*A*A*A/120);
		 yval = M+NN*Math.tan(latitude1)*(A*A/2+(5-T+9*C+4*C*C)*A*A*A*A/24
		 +(61-58*T+T*T+600*C-330*ee)*A*A*A*A*A*A/720);
		 X0 = 1000000L*(ProjNo+1)+500000L;
		 Y0 = 0;
		 xval = xval+X0; yval = yval+Y0;
		 //*X = xval;
		 //*Y = yval;
		 //System.out.println("x："+xval);
		 //System.out.println("y："+yval);
		 output[0] = xval;
		 output[1] = yval;
		 return output;
	 }
	 public static double getDistance(double[] a, double[] b){
		double _x = Math.abs(a[0] - b[0]);
		double _y = Math.abs(a[1] - b[1]);
		return Math.sqrt(_x*_x+_y*_y);
	 }
	 public static double getAngle(double[] a, double[] b){
		double _l = getDistance(a,b);
		double _y = (a[1] - b[1]);
		return Math.asin(_y/_l);
	 }
	 //clockwise
	 public static void rotate(double[] a, double angle){
		 double x = a[0]*Math.cos(angle)+a[1]*Math.sin(angle);
		 double y = -a[0]*Math.sin(angle)+a[1]*Math.cos(angle);
		 a[0]=x;
		 a[1]=y;
	 }
	 //clockwise
	 public static void rotate(double[] a, double[] center,double angle){
		 double x = (a[0]-center[0])*Math.cos(angle)+(a[1]-center[1])*Math.sin(angle)+center[0];
		 double y =-(a[0]-center[0])*Math.sin(angle)+(a[1]-center[1])*Math.cos(angle)+center[1];
		 a[0]=x;
		 a[1]=y;
	 }
	 public static void move(double[] a, double[] b){
		 double x = a[0]+b[0];
		 double y = a[1]+b[1];
		 a[0]=x;
		 a[1]=y;
	 }
	 
}
