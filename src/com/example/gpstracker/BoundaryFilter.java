package com.example.gpstracker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.location.Location;

public class BoundaryFilter {
	File mGpsBorderFile=null;
	FileOutputStream mWriteStream = null;
	FileInputStream mReadStream = null;
	double[] left_top=new double[2];
	double[] left_bottom=new double[2];
	double[] right_top=new double[2];
	double[] right_bottom=new double[2];
	double width;
	double height;
	int image_width;
	int image_height;	
	boolean switchxy;
	double scaling_rate_x;
	double scaling_rate_y;
	double angle_rotate;
	List<XY_Point> pointlist = new ArrayList<XY_Point>();
	
	public class PlanePosition {
		int x;
		int y;
		int weight;
		public PlanePosition(int x,int y){
			this.x = x;
			this.y = y;
			weight = 0;
		}
	}
	
	class XY_Point{
		double x;
		double y;
		XY_Point(){
			x=0;
			y=0;
		}
		XY_Point(double x,double y){
			this.x=x;
			this.y=y;
		}
		XY_Point(XY_Point old){
			this.x = old.x;
			this.y = old.y;
		}
	}
	
	BoundaryFilter(File file, int image_width, int image_height){
		mGpsBorderFile = file;
		switchxy = false;
		this.image_width = image_width;
		this.image_height = image_height;
	}
	public void clean(){
		if(mWriteStream!=null){
			try {
				mWriteStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mWriteStream = null;
		}
	}
	public void addGpsData(double[] xy){
		if(mWriteStream==null){
			try {
				mWriteStream = new FileOutputStream(mGpsBorderFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			mWriteStream.write(CommonUtil.getBytes(xy[0]));
			mWriteStream.write(CommonUtil.getBytes(xy[1]));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	public boolean analyze() throws Exception{
		boolean result = true;
		if(mWriteStream!=null){
			try {
				mWriteStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = false;
			}
			mWriteStream = null;
		}
		try {
			mReadStream = new FileInputStream(mGpsBorderFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		}
		
		if(mReadStream!=null){
			//handle data
			byte[] inOutb = null;
			try {
				inOutb = new byte[mReadStream.available()];
				mReadStream.read(inOutb);
				
				if(inOutb!=null && inOutb.length>80){					
					for(int i=0,n=inOutb.length/16;i<n;i++){
						XY_Point point = new XY_Point();
						point.x = CommonUtil.getDouble(inOutb,i*16);
						point.y = CommonUtil.getDouble(inOutb,(i*16+8));
						pointlist.add(point);
					}
					sort(pointlist);				
					pointlist = convexHull(pointlist);		
					for(int i=0,n=pointlist.size();i<n;i++){
	        			System.out.println("corners x:"+pointlist.get(i).x+" y:"+pointlist.get(i).y);
	        		}
					if(pointlist.size()<4){
						System.out.println("too little corner");
						return false;
					}
					
					if(!rotatingCalipers(pointlist)){
						System.out.println("failed to rotatingCalipers");
						return false;
					}
					
					//get angle
					angle_rotate = CommonUtil.getAngle(right_top,left_top);
					//get width and height
					width  = CommonUtil.getDistance(left_top,right_top);
					height = CommonUtil.getDistance(left_top,left_bottom);
					if(height>width){
						switchxy = true;
						double tmp = width;
						width = height;
						height=tmp;
					}					
					scaling_rate_y =  image_height/height;
					scaling_rate_x =  image_width/width;			
				/*
					double[] test = new double[2];
					test[0] = right_bottom[0] - left_top[0]>0?1:-1;
					test[1] = right_bottom[1] - left_top[1]>0?1:-1;;
					if(adjust_xy(test)<0){
						angle_rotate = -angle_rotate;
						test[0] = right_bottom[0] - left_top[0]>0?1:-1;
						test[1] = right_bottom[1] - left_top[1]>0?1:-1;;
						if(adjust_xy(test)<0){
							result = false;
							throw new Exception("angle_rotate error");
						}
					}
					*/
					//CommonUtil.rotate(right_top, left_top,angle_rotate);
					//CommonUtil.rotate(left_bottom, left_top,angle_rotate);				
				}
				else{
					result = false;
				}
				mReadStream.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result = false;
			} 			
		}
		mReadStream = null;
		return result;
	}
	public int adjust_xy(double[] xy){
		xy[0] = xy[0]-left_top[0];
		xy[1] = xy[1]-left_top[1];
		CommonUtil.rotate(xy,angle_rotate);
		if((xy[0]<0)||(xy[1]<0)){
			return -1;
		}
		if(switchxy){
			double tmp = xy[0];
			xy[0]=xy[1];
			xy[1]=tmp;
		}		
		if( (xy[0]>width) || (xy[1]>height)){
			return -2;
		}
		xy[0] = xy[0]*scaling_rate_x;
		xy[1] = xy[1]*scaling_rate_y;
		return 0;
	}
	/////////////////////////////////////////////////////////////////
	//RotatingCalipers
	//x1*y2-x2*y1 > 0 ====>x1/y1>x2/y2
	public static double xmult(XY_Point p1,XY_Point p2,XY_Point p0){
	    return ((p1.x-p0.x)*(p2.y-p0.y)-(p2.x-p0.x)*(p1.y-p0.y));
	}
	public void sort(List<XY_Point> points){
		XY_Point base = new XY_Point();
		//Filter point
		//get distence between two points.
		for(int i=0,n=points.size();i<n;){
			XY_Point p = points.get(i);
			double d = dist(p,points.get((i+1)%n));
			if(d>10000){
				points.remove(i);
				n--;
			}
			else{
				i++;
			}
		}		
		
		XY_Point p0 = points.get(0);
		base.x = p0.x;
		base.y = p0.y;
		
		int index = 0;
		//first get the bottom of all as base point;
		for(int i=1,n=points.size();i<n;i++){
			XY_Point p = points.get(i);
			Double y = p.y;
			if(y.compareTo(base.y)<0){
				base.x = p.x;
				base.y = p.y;		
				index = i;
			}
		}
		//put the first one as base point
		Collections.swap(points, 0, index);
		//order all the point by triangle area
	    for(int j=points.size()-1;j>=1;j--){
	        for(int i=1;i<j;i++){
	            Double xm=xmult(points.get(i),points.get(i+1),base);
	            if(xm.compareTo((double) 0)<0){
	            	Collections.swap(points, i, i+1);
	            }
	        }
	    }
	}
	double dist(XY_Point p1,XY_Point p2){
	    return (p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y);
	}
	double dist(double[] p1,double[] p2){
	    return (p2[0]-p1[0])*(p2[0]-p1[0])+(p2[1]-p1[1])*(p2[1]-p1[1]);
	}
	
	//get all corners
	public List<XY_Point> convexHull(List<XY_Point> points){
		List<XY_Point> pstack = new ArrayList<XY_Point>();
	    sort(points);
	    
	    pstack.add(points.get(0));
	    pstack.add(points.get(1));
	    int stack_top=pstack.size()-1;
	    
	    for(int i=2;i<points.size();i++){
	        stack_top=pstack.size()-1;
	        XY_Point pt2=pstack.get(stack_top);
	        XY_Point pt1=pstack.get(stack_top-1);
	        XY_Point ptcur=points.get(i);
	        
	        int xm=(int) ((pt2.x-pt1.x)*(ptcur.y-pt2.y)-(ptcur.x-pt2.x)*(pt2.y-pt1.y));
	        while(xm<=0 && pstack.size()>2){           
	            pstack.remove(stack_top);
	            stack_top=pstack.size()-1;
	            pt2=pstack.get(stack_top);
	            pt1=pstack.get(stack_top-1);
	            xm= (int) ((pt2.x-pt1.x)*(ptcur.y-pt2.y)-(ptcur.x-pt2.x)*(pt2.y-pt1.y));
	        }
	        pstack.add(points.get(i));
	    }
	    return pstack;
	}
	//
	boolean rotatingCalipers(List<XY_Point> corners){
	    int q=1;
	    int maxLength=0;
	    int size = corners.size();
	    corners.add(corners.get(0));
	    int max = 0;
	    List<XY_Point> rect_point = new ArrayList<XY_Point>();
	    
	    int index1 = 0;
	    int index2 = 0;
	    int index3 = 0;
	    int index4 = 0;
	    
	    double max_cross_line = 0;
	    for(int i=0;i<size;i++){
	    	//get the bigest triangle area
	        while(Math.abs(xmult(corners.get(i+1),corners.get(q+1),corners.get(i)))>Math.abs(xmult(corners.get(i+1),corners.get(q),corners.get(i)))){
	            q=(q+1)%size;
	        }
	        
	        Double d1=dist(corners.get(i),corners.get(q));
	        Double d2=dist(corners.get(i+1),corners.get(q));
	        if(d1.compareTo(d2)>0 && d1.compareTo(max_cross_line)>0){
	        	max_cross_line = d1;
	        	index1 = i;
	        	index2 = q;
	        }else if(d2.compareTo(d1)>0 && d2.compareTo(max_cross_line)>0){
	        	max_cross_line=d2;
	        	index1 = i+1;
	        	index2 = q;
	        }
	    }
	    
	    XY_Point corner1 = new XY_Point(corners.get(index1));
	    XY_Point corner2 = new XY_Point(corners.get(index2));
	    
	    XY_Point center= new XY_Point((corner1.x+corner2.x)/2,(corner1.y+corner2.y)/2);
	    max_cross_line = max_cross_line/4;
	    
	    double o_area = 0;
	    
	    for(int i=0;i<size;i++){
	    	//get the bigest triangle area
	        Double temp = Math.abs(xmult(corner1,corners.get(i),corner2));
	        if(temp.compareTo(o_area)>0){
	        	o_area = temp; 
	        	index3 = i;
	        }
	    }
	    XY_Point corner3 = new XY_Point(corners.get(index3));
	    
	    double k = (corner3.y-center.y)/(corner3.x-center.x);
    	corner3.x = (center.x+ Math.sqrt(max_cross_line/(1+k*k)));		    
	    corner3.y = k*(corner3.x-center.x)+center.y;
	    
	    XY_Point corner4 = new XY_Point();
	    corner4.x = center.x*2-corner3.x;		
	    corner4.y = center.y*2-corner3.y;	
	    
	    rect_point.clear();
	    rect_point.add(corner1);
	    rect_point.add(corner2);
	    rect_point.add(corner3);
	    rect_point.add(corner4);	    
	    
	    int selected = 0;
	    left_bottom[0]=right_top[0]=right_bottom[0]=0;
	    left_bottom[1]=right_top[1]=right_bottom[1]=0;
	    left_top[0]=FLT_MAX;
	    left_top[1]=FLT_MAX;
	    
	    for(int i=0,n=rect_point.size();i<n;i++){
			XY_Point p = rect_point.get(i);
			Double y = p.y;
			if(y.compareTo(left_top[1])<0){
				left_top[1] = p.y;
				left_top[0] = p.x;	
				selected = i;
			}
		}
	    XY_Point base = rect_point.get(selected);
	    rect_point.remove(selected);
	    selected = 0;
	    max_cross_line = 0;
	    for(int i=0,n=rect_point.size();i<n;i++){
	    	Double d1=dist(base,rect_point.get(i));
			if(d1.compareTo(max_cross_line)>0){
				max_cross_line = d1;
				selected = i;
			}
		}
	    right_bottom[0] = rect_point.get(selected).x;
	    right_bottom[1] = rect_point.get(selected).y;	    
	    rect_point.remove(selected);
	    //bottom>top
	    //right>left
	    Double x = rect_point.get(0).x;
	    if(rect_point.get(0).x<rect_point.get(1).x){
			left_bottom[1] = rect_point.get(0).y;
			left_bottom[0] = rect_point.get(0).x;
			right_top[1] = rect_point.get(1).y;
			right_top[0] = rect_point.get(1).x;
	    }
	    else{
	    	left_bottom[1] = rect_point.get(1).y;
			left_bottom[0] = rect_point.get(1).x;
			right_top[1] = rect_point.get(0).y;
			right_top[0] = rect_point.get(0).x;
	    }
		return true;
	}	
	/*
	boolean rotatingCalipers_old(List<XY_Point> corners){
	    int q=1;
	    int maxLength=0;
	    int size = corners.size();
	    corners.add(corners.get(0));
	    double max = 0;
	    List<XY_Point> rect_point = new ArrayList<XY_Point>();
	      	    
	    for(int i=0;i<size;i++){
	    	//get the bigest triangle area
	        while(xmult(corners.get(i+1),corners.get(q+1),corners.get(i))>xmult(corners.get(i+1),corners.get(q),corners.get(i))){
	            q=(q+1)%size;
	        }
	        double current = xmult(corners.get(i+1),corners.get(q),corners.get(i));
	        if(max<current){
	        	max = current;
	        	rect_point.clear();
	        	rect_point.add(corners.get(i));
	        	rect_point.add(corners.get(i+1));	        	
	        	rect_point.add(corners.get(q));
	        }
	    }
	    if(rect_point.size()<3){
	    	return false;
	    }
	    for(int i=0,n=rect_point.size();i<n;i++){
			System.out.println("rect_point x:"+rect_point.get(i).x+" y:"+rect_point.get(i).y);
		}	    
	    
	    //TODO: find the outside line as base line, not diagonally
	    XY_Point first = rect_point.get(0);
	    XY_Point second = rect_point.get(1);
	    XY_Point top_one = new XY_Point(rect_point.get(2));
	    XY_Point left_corner = rect_point.get(2);
	    XY_Point right_corner = rect_point.get(2);
	    
	    double d = dist(first,second);	
	    //height
	    height = (int) (xmult(second,top_one,first)/(Math.sqrt(d)));
	    	    
	    double max_r = 0;
	    double max_l = 0;
	    for(int i=0;i<size;i++){
	    	double area = xmult(second,corners.get(i),first);
	    	//get the longest dist  with left corner and right corner in  shadow
	    	//to find left point and right point
	    	double dist_r = dist(first,corners.get(i));
	    	double dist_l = dist(second,corners.get(i));	
	    	double temp = dist_r - area*area/d;
	    	if(temp>max_r){
	    		max_r = temp;
	    		right_corner = corners.get(i);
	    	}
	    	temp = dist_l - area*area/d;
	    	if(temp>max_l){
	    		max_l = temp;
	    		left_corner = corners.get(i);
	    	}	        
	    }
	    //System.out.println("left corner x:"+left_corner.x+" y:"+left_corner.y);
	    //System.out.println("right corner x:"+right_corner.x+" y:"+right_corner.y);
	    
	    //get slope of line
	    double k  = (second.y-first.y)/(second.x-first.x);
	    if(k>0){
		    //we have length and k, and one point to get target point in line
	    	right_top[0] = (int) (first.x+Math.sqrt(max_r/(1+k*k)));		    
	    	left_top[0] = (int) (second.x-Math.sqrt(max_l/(1+k*k)));
	    }
	    else{
	    	right_top[0] = (int) (first.x-Math.sqrt(max_r/(1+k*k)));
	    	left_top[0] = (int) (second.x+Math.sqrt(max_l/(1+k*k)));
	    }
	    right_top[1] = (int) (k*(right_top[0]-first.x)+first.y);
	    left_top[1] = (int) (k*(left_top[0]-second.x)+second.y);
	    
	  //now we have two corner
	    System.out.println("prepare left_top x:"+left_top[0]+" y:"+left_top[1]);
	    System.out.println("prepare right_top x:"+right_top[0]+" y:"+right_top[1]);
	    
	    width = (int) Math.sqrt(dist(left_top,right_top));
	    
	    if(k>0){
	    	//first get height length in line, then switch PI/2 to get target point
	    	right_bottom[0] = (int) (right_top[0]+Math.sqrt(height*height/(1+k*k)));		    
	    	left_bottom[0] = (int) (left_top[0]+Math.sqrt(height*height/(1+k*k)));
	    }
	    else{
	    	right_bottom[0] = (int) (right_top[0]-Math.sqrt(height*height/(1+k*k)));		    
	    	left_bottom[0] = (int) (left_top[0]-Math.sqrt(height*height/(1+k*k)));
	    }
	    right_bottom[1] = (int) (k*(right_bottom[0]-right_top[0])+right_top[1]);
	    left_bottom[1] = (int) (k*(left_bottom[0]-left_top[0])+left_top[1]);
	    CommonUtil.rotate(right_bottom, right_top, -Math.PI/2);
	    CommonUtil.rotate(left_bottom, left_top, -Math.PI/2);

	    //now we have four corner
	    System.out.println("prepare right_bottom x:"+right_bottom[0]+" y:"+right_bottom[1]);
	    System.out.println("prepare left_bottom x:"+left_bottom[0]+" y:"+left_bottom[1]);
	    
	    //find right place for four corner
	    rect_point.clear();
	    rect_point.add(new XY_Point((int)left_top[0],(int)left_top[1]));
	    rect_point.add(new XY_Point((int)right_top[0],(int)right_top[1]));
	    rect_point.add(new XY_Point((int)right_bottom[0],(int)right_bottom[1]));
	    rect_point.add(new XY_Point((int)left_bottom[0],(int)left_bottom[1]));
	    
	    //correct four corner's position
	    int selected = 0;
	    for(int i=0,n=rect_point.size();i<n;i++){
			XY_Point p = rect_point.get(i);
			if(p.y < left_top[1] || (p.y==left_top[1] && p.x<left_top[0])){
				left_top[1] = p.y;
				left_top[0] = p.x;	
				selected = i;
			}
		}
	    XY_Point base = rect_point.get(selected);
	    rect_point.remove(selected);
	    selected = 0;
	    double max_cross_line = 0;
	    for(int i=0,n=rect_point.size();i<n;i++){
			XY_Point p = rect_point.get(i);
			double d1=dist(base,corners.get(i));
			if(d1>max_cross_line){
				max_cross_line = d1;
				selected = i;
			}
		}
	    right_bottom[0] = rect_point.get(selected).x;
	    right_bottom[1] = rect_point.get(selected).y;	    
	    rect_point.remove(selected);
	    if(rect_point.get(0).x<rect_point.get(1).x){
			left_bottom[1] = rect_point.get(0).y;
			left_bottom[0] = rect_point.get(0).x;
			right_top[1] = rect_point.get(1).y;
			right_top[0] = rect_point.get(1).x;
	    }
	    else{
	    	left_bottom[1] = rect_point.get(0).y;
			left_bottom[0] = rect_point.get(0).x;
			right_top[1] = rect_point.get(1).y;
			right_top[0] = rect_point.get(1).x;
	    }
		
		return true;
	}	
	*/
	public Bitmap loadLocationData(List<PlanePosition> l,Bitmap bm){
    	//spreadData = new HashMap<PlanePosition,Integer>();
		List<PlanePosition> rawData = l;
    	int height = bm.getHeight();
 		int width = bm.getWidth();
 		if(rawData.size()<5)
 			return bm;
 		
    	//preprocess input data
    	Iterator<PlanePosition> sListIterator = rawData.iterator();  
    	
    	while(sListIterator.hasNext()){  
    		PlanePosition e = sListIterator.next();
			//System.out.println("scan x:"+e.x+" y:"+e.y);
    		if(e.x>=width || e.y >= height || e.x<0 || e.y<0){
    			//System.out.println("remove x:"+e.x+" y:"+e.y);
    			sListIterator.remove();    			
    			continue;
    		}
    	} 
    	
    	int size = rawData.size();
    	
    	byte[][] pData = new byte[bm.getWidth()][bm.getHeight()]; 
    	//generate raw data
    	Random random = new Random();
    	int range = 20;
        
 		
 		long begin = System.currentTimeMillis();
		for(PlanePosition p:rawData){
			int x = p.x;
			int y = p.y;
			int weight = 1;
			int col = pData[x][y];
			if(col==0){	
				for(int i=x-range/2;(i<x+range/2)&&(i>0)&&(i<width);i++){
					for(int j=y-range/2;(j<y+range/2)&&(j>0)&&(j<height);j++){
						col = pData[i][j];
						if(col>0){
							weight++;
							pData[i][j] = (byte) ((col+1)%255);
						}
					}
				}
				pData[x][y] = (byte) (weight%255);
			}
			else{
				pData[x][y] = (byte) ((col+1)%255);
			}
		}
		//find the bigest weight. 
		for(PlanePosition p:rawData){
			int x = p.x;
			int y = p.y;
			p.weight = pData[x][y]; 
		}
		Collections.sort(rawData, new Comparator<PlanePosition> (){
			@Override
			public int compare(PlanePosition arg0, PlanePosition arg1) {
				// TODO Auto-generated method stub
				return arg0.weight-arg1.weight;
			}			
		});
		Canvas canvas = new Canvas(bm);
 		canvas.drawBitmap(bm, 0, 0, null);
		int max = rawData.get(size-1).weight;
		//System.out.println("max:"+max);
		for(PlanePosition p:rawData){
			int x = p.x;
			int y = p.y;
			Paint paint = new Paint();
			RadialGradient rGradient = new RadialGradient(x,y,range*2,  
                    Color.argb(p.weight*255/max, 255,255,255),Color.argb(0, 0,255,0),Shader.TileMode.CLAMP);  
			paint.setShader(rGradient);
			canvas.drawCircle(x, y, range*2, paint);
		}

 		return bm;
    }
	///////////////////////////////////////////////////////////////////////////////
	/* min positive value */
	final float FLT_MIN = 1.175494351e-38F; 
	/* max value */ 
	final float FLT_MAX = 3.402823466e+38F; 
	/* smallest such that 1.0+FLT_EPSILON != 1.0 */ 
	final float FLT_EPSILON = 1.192092896e-07F; 
	/*
	boolean icvRotatingCalipers( List<XY_Point> points )
	{
	    float minarea = FLT_MAX;
	    float max_dist = 0;
	    int[] buffer = new int[8];
	    int i, k;
	    List<XY_Point> vect = new ArrayList<XY_Point>();
	    List<Integer>inv_vect_length = new ArrayList<Integer>();
	    int left = 0, bottom = 0, right = 0, top = 0;
	    int[] seq = { -1, -1, -1, -1 };
	   
	    float orientation = 0;
	    float base_a;
	    float base_b = 0;
	    float left_x, right_x, top_y, bottom_y;
	    XY_Point pt0 = points.get(0);
	    int n = points.size();
	    
	    left_x = right_x = pt0.x;
	    top_y = bottom_y = pt0.y;
	    
	    for( i = 0; i < n; i++ )
	    {
	        if( pt0.x < left_x ){
	            left_x = pt0.x;
	            left = i;
	        }
	        if( pt0.x > right_x ){
	            right_x = pt0.x;
	            right = i;
	        }
	        if( pt0.y > top_y ){
	            top_y = pt0.y;
	        	top = i;
	    	}
	        if( pt0.y < bottom_y ){
	            bottom_y = pt0.y;
	            bottom = i;
	        }
	        XY_Point pt = points.get((i+1) & (i+1 < n ? -1 : 0));
	        vect.add(new XY_Point((pt.x - pt0.x),(pt.y - pt0.y)));
	        inv_vect_length.add((int) Math.sqrt((pt.x - pt0.x)*(pt.x - pt0.x) + (pt.y - pt0.y)*(pt.y - pt0.y)));
	        pt0 = pt;
	    }
	    {
	        double ax = vect.get(n-1).x;
	        double ay = vect.get(n-1).y;
	        
	        for( i = 0; i < n; i++ )
	        {
	            double bx = vect.get(i).x;
	            double by = vect.get(i).y;
	            double convexity = ax * by - ay * bx;
	            if( convexity != 0 )
	            {
	                orientation = (convexity > 0) ? 1.f : (-1.f);
	                break;
	            }
	            ax = bx;
	            ay = by;
	        }
	        assert( orientation != 0 );
	    }
	    base_a = orientation;
	    seq[0] = bottom;
	    seq[1] = right;
	    seq[2] = top;
	    seq[3] = left;
	    for( k = 0; k < n; k++ )
	    {
	        float sinus;
	        float dp0 = base_a * vect.get(seq[0]).x + base_b * vect.get(seq[0]).y;
	        float dp1 = -base_b * vect.get(seq[1]).x + base_a * vect.get(seq[1]).y;
	        float dp2 = -base_a * vect.get(seq[2]).x - base_b * vect.get(seq[2]).y;
	        float dp3 = base_b * vect.get(seq[3]).x - base_a * vect.get(seq[3]).y;
	        float cosalpha = dp0 * inv_vect_length.get(seq[0]);
	        float maxcos = cosalpha;
	        int main_element = 0;
	        cosalpha = dp1 * inv_vect_length.get(seq[1]);
	        if(cosalpha > maxcos){
	        	main_element = 1;
	        	maxcos = cosalpha;
	        }	        
	        cosalpha = dp2 * inv_vect_length.get(seq[2]);
	        if(cosalpha > maxcos){
	        	main_element = 2;
	        	maxcos = cosalpha;
	        }
	        cosalpha = dp3 * inv_vect_length.get(seq[3]);
	        if(cosalpha > maxcos){
	        	main_element = 3;
	        	maxcos = cosalpha;
	        }	        
	        sinus = (float) (orientation * Math.sqrt( 1 - maxcos * maxcos ));
	        {
	            float x = base_a;
	            float y = base_b;
	            base_a = maxcos * x - sinus * y;
	            base_b = sinus * x + maxcos * y;
	        }
	        seq[main_element] += 1;
	        seq[main_element] = (seq[main_element] == n) ? 0 : seq[main_element];
            {
                float height;
                float area;
                float dx = points.get(seq[1]).x - points.get(seq[3]).x;
                float dy = points.get(seq[1]).y - points.get(seq[3]).y;
                float width = dx * base_a + dy * base_b;
                dx = points.get(seq[2]).x - points.get(seq[0]).x;
                dy = points.get(seq[2]).y - points.get(seq[0]).y;
                height = -dx * base_b + dy * base_a;
                area = width * height;
                if( area <= minarea )
                {
                    int[] buf = buffer;
                    minarea = area;
                    
                    buf[0] = seq[3];
                    buf[1] = (int) base_a;
                    buf[2] = (int) width;
                    buf[3] = (int) base_b;
                    buf[4] = (int) height;
                  
                    buf[5] = seq[0];
                    buf[6] = (int) area;
                }
            }
	    } 
        {
        	int[] buf = buffer;
        	int A1 = buf[1];
        	int B1 = buf[3];
        	int A2 = -buf[3];
        	int B2 = buf[1];
        	int C1 = A1 * points.get(buf[0]).x + points.get(buf[0]).y * B1;
        	int C2 = A2 * points.get(buf[5]).x + points.get(buf[5]).y * B2;
        	int idet = (int) (1.f / (A1 * B2 - A2 * B1));
        	int px = (C1 * B2 - C2 * B1) * idet;
        	int py = (A1 * C2 - A2 * C1) * idet;
        	
        	left_top[0] = px;
        	left_top[1] = py;
        	right_top[0] = px + A1 * buf[2];
        	right_top[1] = py + B1 * buf[2];
            left_bottom[0] = px + A2 * buf[4];
            left_bottom[1] = py + B2 * buf[4];
        }
		return true;
	}
	*/
	
}
